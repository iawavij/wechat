package kuma.exception;

import com.alibaba.fastjson.JSON;
import com.kuma.pojo.Result;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class ExceptionHandler implements HandlerExceptionResolver {

    private static Logger logger = Logger.getLogger(ExceptionHandler.class);

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response,
            Object handler, Exception ex) {
        if (!(ex instanceof AppException)) {
            logger.error("系统严重错误", ex);
        }

        String accept = request.getHeader("accept");
        String x_requested_with = request.getHeader("x-requested-with");

        Result rs = Result.error(ex.getMessage());

        if (accept.startsWith("application/json")) {
            response.setContentType("application/json;charset=UTF-8");
            try (PrintWriter writer = response.getWriter()) {
                writer.write(JSON.toJSONString(rs));
            } catch (IOException ex1) {
                logger.error(String.format("输出异常信息出错：\n%s", ex1.getMessage()), ex1);

            }
            return null;
        } else {
            return new ModelAndView("error", rs);
        }
    }

}
