package kuma.utils;

public class Constants {

    public static final String VERSION = "1.0.0_20150726";

    public static final String SESSION_USER = "loginUser";
    public static final String SESSION_CODE = "verifyCode";
    public static final String SESSION_LOCKSCREEN = "lockscreen";
}
