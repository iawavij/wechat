/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.listener;

import com.mysql.jdbc.AbandonedConnectionCleanupThread;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.apache.log4j.Logger;

/**
 *
 * 三个域对象的创建与销毁 ServletRequestListener, ServletContextListener,
 * HttpSessionListener,
 *
 * 三个域对象的属性变化 ServletRequestAttributeListener, HttpSessionAttributeListener,
 * ServletContextAttributeListener
 *
 * 用于通知被绑定到Session对象的属性 HttpSessionBindingListener 哪个对象被绑定哪个对象去实现
 *
 * 用于监听Session的钝化与活化 HttpSessionActivationListener
 */
@WebListener
public class MyListener implements ServletContextListener {

    private Logger logger = Logger.getLogger(getClass());

    @Override
    public void contextInitialized(ServletContextEvent sce) {
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver dr = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(dr);
                logger.info(String.format("撤销注册 jdbc driver: %s", dr));
            } catch (SQLException ex) {
                logger.error(String.format("撤销注册出错 jdbc driver %s", dr), ex);
            }
        }

        //释放 MySql AbandonedConnectionCleanupThread 线程
        try {
            AbandonedConnectionCleanupThread.shutdown();
            logger.info(String.format("释放 MySql AbandonedConnectionCleanupThread 线程"));
        } catch (InterruptedException ex) {
            logger.error(String.format("关闭AbandonedConnectionCleanupThread线程出错：\n%s", ex.getMessage()), ex);
        }
    }
}
