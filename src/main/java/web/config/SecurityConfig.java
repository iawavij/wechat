/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 *
 * @author Kuma
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        // 设置不拦截规则
        web.ignoring().antMatchers(HttpMethod.GET, "/static/**", "/dist/**");
    }

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        // 设置拦截规则  
//
//        // 开启默认登录页面
//        // http.formLogin();
//        // 自定义登录页面  
//        http.csrf().disable().formLogin()
//                .loginPage("/login")
//                .failureUrl("/login?error=1")
//                .loginProcessingUrl("/j_spring_security_check")
//                .usernameParameter("j_username")
//                .passwordParameter("j_password").permitAll();
//
//        // 自定义注销  
//        http.logout().logoutUrl("/logout").logoutSuccessUrl("/login")
//                .invalidateHttpSession(true);
//
//        // session管理  
//        http.sessionManagement().sessionFixation().changeSessionId()
//                .maximumSessions(1).expiredUrl("/");
//
//        // 记住登录用户  
//        http.rememberMe().key("webmvc#FD637E6D9C0F1A5A67082AF56CE32485");
//    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 自定义UserDetailsService
        auth.userDetailsService(userDetailsService()).passwordEncoder(
                new Md5PasswordEncoder());
    }

}
