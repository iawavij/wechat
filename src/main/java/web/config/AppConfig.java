/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.config;

import com.kuma.wechat4j.common.RequestData;
import kuma.exception.ExceptionHandler;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;

//@Configuration ： 类似于spring配置文件，负责注册bean，对应的提供了@Bean注解。需要org.springframework.web.context.support.AnnotationConfigWebApplicationContext注册到容器中。
//@ComponentScan ： 注解类查找规则定义 <context:component-scan/>
//@EnableAspectJAutoProxy ： 激活Aspect自动代理 <aop:aspectj-autoproxy/>
//@Import @ImportResource: 关联其它spring配置  <import resource="" />
//@EnableCaching ：启用缓存注解  <cache:annotation-driven/>
//@EnableTransactionManagement ： 启用注解式事务管理 <tx:annotation-driven />
//@EnableWebMvcSecurity ： 启用springSecurity安全验证
/**
 *
 * @author kuma
 */
@Configuration
@ComponentScan(basePackages = "com.kuma", excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ANNOTATION, value = {Controller.class})})
@Import({DaoConfig.class})
public class AppConfig {

    @Bean
    public static PropertyPlaceholderConfigurer ppc() {
        PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
        ppc.setLocations(new Resource[]{
            new ClassPathResource("jdbc.properties"),
            new ClassPathResource("tuling.properties")
        });
        return ppc;
    }

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer msc = new MapperScannerConfigurer();
        msc.setBasePackage("com.kuma.mapper");
        msc.setSqlSessionFactoryBeanName("sqlSessionFactory");
        return msc;
    }

    @Bean
    public RequestData requestData() {
        return new RequestData();
    }

    @Bean
    public ExceptionHandler exceptionHandler() {
        return new ExceptionHandler();
    }
}
