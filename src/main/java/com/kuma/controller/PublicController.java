/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.controller;

import com.kuma.pojo.Result;
import com.kuma.pojo.UserInfo;
import com.kuma.pojo.UserInfoCustom;
import com.kuma.service.UserInfoService;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import kuma.exception.AppException;
import kuma.utils.Constants;
import kuma.utils.VerifyCode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Kuma
 */
@Controller
public class PublicController {

    @Resource
    private UserInfoService userInfoService;

    @RequestMapping(value = "code.png", method = RequestMethod.GET)
    public void createCode(HttpServletRequest req, HttpServletResponse resp, HttpSession session) throws IOException {
        VerifyCode vf = new VerifyCode();
        BufferedImage buffImg = vf.getImage();

//        保存到Session
        session.setAttribute(Constants.SESSION_CODE, vf.getText());

//        禁止图像缓存
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Cache-Control", "no-cache");
        resp.setDateHeader("Expires", 0);
        resp.setContentType("image/png");

        // 将图像输出到Servlet输出流中。
        try (ServletOutputStream sos = resp.getOutputStream()) {
            ImageIO.write(buffImg, "png", sos);
        }
    }

    @RequestMapping(value = {"login"}, method = RequestMethod.POST)
    public @ResponseBody
    Object Login(@RequestBody UserInfoCustom user, HttpSession session) throws Exception {
        String vcode = (String) session.getAttribute(Constants.SESSION_CODE);
        session.removeAttribute(Constants.SESSION_CODE);    //提交登录就移除session里的验证码,防止机器人提交

        if (vcode == null) {
            throw new AppException("验证码不存在！");
        } else if (!user.getVcode().equalsIgnoreCase(vcode)) {
            throw new AppException("验证码错误！");
        }

        String code = user.getCode();
        String password = user.getPwd();

        if (code == null || password == null) {
            throw new AppException("用户名密码不能为空！");
        }

        UserInfo u = userInfoService.login(code, password);
        session.removeAttribute(Constants.SESSION_LOCKSCREEN);
        session.setAttribute(Constants.SESSION_USER, u);
        return Result.success();
    }
}
