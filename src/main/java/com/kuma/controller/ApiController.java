/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.controller;

import com.kuma.service.ApiService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Kuma
 */
@Controller
@RequestMapping(value = {"api"})
public class ApiController {

    @Resource
    private ApiService service;

    //http://localhost:8080/wechat/api?signature=b64df2f24e987b2c828a12301b718c17a111f2f2&timestamp=1467012722&nonce=46405507&echostr=6824005983639560062
    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String execute(HttpServletRequest request) {
        return service.execute(request);
    }

    @RequestMapping(value = {"test"}, method = {RequestMethod.GET})
    public void oauth2(String code, String state) {
        System.out.println(code);
        System.out.println(state);
    }
}
