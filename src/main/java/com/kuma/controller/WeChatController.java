/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.controller;

import com.kuma.service.WeChatService;
import com.kuma.wechat4j.model.menu.CreateMenuInfo;
import javax.annotation.Resource;
import kuma.exception.AppException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Kuma
 */
@Controller
@RequestMapping(value = {"wechatmanage"})
public class WeChatController {

    @Resource
    private WeChatService service;

    @RequestMapping(value = {"menu"}, method = RequestMethod.GET)
    public String toMenu() {
        return "wechat/test";
    }

    @RequestMapping(value = {"getcallbackip"}, method = RequestMethod.GET)
    public @ResponseBody
    Object getCallBackIp() throws AppException {
        return service.getCallBackIp();
    }

    @RequestMapping(value = {"menu_create"}, method = RequestMethod.POST)
    public @ResponseBody
    Object menuCreate(@RequestBody CreateMenuInfo button) throws AppException {
        return service.menuCreate(button);
    }

    @RequestMapping(value = {"menu_get"}, method = RequestMethod.GET)
    public @ResponseBody
    Object menuGet() throws AppException {
        return service.menuGet();
    }

    @RequestMapping(value = {"menu_delete"}, method = RequestMethod.GET)
    public @ResponseBody
    Object menuDelete() throws AppException {
        return service.menuDelete();
    }

    @RequestMapping(value = {"get_current_selfmenu_info"}, method = RequestMethod.GET)
    public @ResponseBody
    Object getCurrentSelfmenuInfo() throws AppException {
        return service.getCurrentSelfmenuInfo();
    }
}
