/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.service;

import com.kuma.tuling.TLRequest;
import com.kuma.tuling.TLService;
import com.kuma.wechat4j.WeChatSupport;
import com.kuma.wechat4j.model.message.req.EventRequestMessage;
import com.kuma.wechat4j.model.message.req.ImageRequestMessage;
import com.kuma.wechat4j.model.message.req.LinkRequestMessage;
import com.kuma.wechat4j.model.message.req.LocationRequestMessage;
import com.kuma.wechat4j.model.message.req.TextRequestMessage;
import com.kuma.wechat4j.model.message.req.VideoRequestMessage;
import com.kuma.wechat4j.model.message.req.VoiceRequestMessage;
import com.kuma.wechat4j.model.message.resp.BaseResponseMessage;
import com.kuma.wechat4j.model.message.resp.ImageResponseMessage;
import com.kuma.wechat4j.model.message.resp.TextResponseMessage;
import com.kuma.wechat4j.model.message.resp.media.Image;
import com.kuma.wechat4j.model.userinfo.UserInfo;
import com.kuma.wechat4j.common.RequestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Kuma
 */
@Service
public class ApiService extends WeChatSupport {

    @Autowired
    private RequestData rut;

    @Autowired
    private TLService tl;

    @Override
    protected BaseResponseMessage onText(TextRequestMessage message) {
        TLRequest tlRequest = new TLRequest();
        tlRequest.setUserid(message.getFromUserName());
        tlRequest.setInfo(message.getContent());
        return tl.post(tlRequest, message);
    }

    @Override
    protected BaseResponseMessage onImage(ImageRequestMessage message) {
        ImageResponseMessage imageMessage = new ImageResponseMessage(message);
        Image image = new Image();
        image.setMediaId(message.getMediaId());
        imageMessage.setImage(image);
        return imageMessage;
    }

    @Override
    protected BaseResponseMessage onVoice(VoiceRequestMessage message) {
        BaseResponseMessage text = null;
        if (message.getRecognition() != null) {
            TLRequest tlRequest = new TLRequest();
            tlRequest.setUserid(message.getFromUserName());
            tlRequest.setInfo(message.getRecognition());
            text = tl.post(tlRequest, message);
        }
        return text;
    }

    @Override
    protected BaseResponseMessage onVideo(VideoRequestMessage message) {
        return null;
    }

    @Override
    protected BaseResponseMessage onLocation(LocationRequestMessage message) {
        return null;
    }

    @Override
    protected BaseResponseMessage onLink(LinkRequestMessage message) {
        return null;
    }

    /**
     * 点击菜单拉取消息时的事件推送
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage CLICK(EventRequestMessage message) {
        return null;
    }

    /**
     * 点击菜单跳转链接时的事件推送
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage VIEW(EventRequestMessage message) {
        return null;
    }

    /**
     * 订阅
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage subscribe(EventRequestMessage message) {
        TextResponseMessage text = new TextResponseMessage(message);

        StringBuilder sb = new StringBuilder("欢迎关注本公众号\n");

        UserInfo user = rut.userInfo(message.getFromUserName());
        if (user.getErrcode() == null || user.getErrcode() == 0) {
            sb.append("昵称：").append(user.getNickname()).append("\n");
            sb.append("性别：").append(user.getSex() == 1 ? "男" : user.getSex() == 0 ? "女" : "未知").append("\n");
            sb.append("地址：").append(user.getCountry()).append("-")
                    .append(user.getProvince()).append("-")
                    .append(user.getCity()).append("\n");
            sb.append("头像：").append(user.getHeadimgurl()).append("\n\n");
        }

        sb.append("我是机器人，可以随便聊骚！还可以查快递，语音消息也可以！").append("\n");
        sb.append("更多功能等你挖掘！");

        text.setContent(sb.toString());
        return text;
    }

    /**
     * 取消订阅
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage unsubscribe(EventRequestMessage message) {
        return null;
    }

    /**
     * 用户已关注时的事件推送
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage SCAN(EventRequestMessage message) {
        return null;
    }

    /**
     * 上报地理位置事件
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage LOCATION(EventRequestMessage message) {
        return null;
    }

    /**
     * 扫码推事件的事件推送
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage scancode_push(EventRequestMessage message) {
        return null;
    }

    /**
     * 扫码推事件且弹出“消息接收中”提示框的事件推送
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage scancode_waitmsg(EventRequestMessage message) {
        return null;
    }

    /**
     * 弹出系统拍照发图的事件推送
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage pic_sysphoto(EventRequestMessage message) {
        return null;
    }

    /**
     * 弹出拍照或者相册发图的事件推送
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage pic_photo_or_album(EventRequestMessage message) {
        return null;
    }

    /**
     * 弹出微信相册发图器的事件推送
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage pic_weixin(EventRequestMessage message) {
        return null;
    }

    /**
     * 弹出地理位置选择器的事件推送
     *
     * @param message
     * @return
     */
    @Override
    protected BaseResponseMessage location_select(EventRequestMessage message) {
        return null;
    }

}
