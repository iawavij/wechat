/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.service;

import com.kuma.mapper.UserInfoMapper;
import com.kuma.pojo.UserInfo;
import com.kuma.pojo.UserInfoExample;
import java.util.List;
import javax.annotation.Resource;
import kuma.exception.AppException;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

@Service
public class UserInfoService {

    @Resource
    private UserInfoMapper mapper;

    private List<UserInfo> findByExample(UserInfoExample example) {
        return this.mapper.selectByExample(example);
    }

    public UserInfo findById(int id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    public UserInfo findByCode(String code) {
        return this.mapper.selectByCode(code);
    }

    public UserInfo login(String code, String password) throws AppException {
        UserInfo user = findByCode(code);
        if (user == null) {
            throw new AppException("用户不存在！");
        }

        String md5_password = DigestUtils.md5Hex(password + user.getSalt());

        if (!md5_password.equals(user.getPassword())) {
            throw new AppException("密码错误！");
        }

        return user;
    }

    public void unLockscreen(UserInfo user, String password) throws Exception {
        if (user == null) {
            throw new AppException("还未登录或当前登录已经超时！");
        }

        String md5_password = DigestUtils.md5Hex(password + user.getSalt());

        if (!md5_password.equals(user.getPassword())) {
            throw new AppException("密码错误！");
        }
    }

}
