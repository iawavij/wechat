/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.service;

import com.kuma.wechat4j.model.CallBackIpModel;
import com.kuma.wechat4j.model.ErrorModel;
import com.kuma.wechat4j.model.MenuGetModel;
import com.kuma.wechat4j.model.SelfMenuInfoModel;
import com.kuma.wechat4j.model.menu.CreateMenuInfo;
import com.kuma.wechat4j.common.RequestData;
import kuma.exception.AppException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeChatService {

    @Autowired
    private RequestData rut;

    /**
     * 获取微信服务器列表
     *
     * @return
     * @throws AppException
     */
    public Object getCallBackIp() throws AppException {
        CallBackIpModel rs = rut.getCallBackIp();
        if (rs.getErrcode() != null && rs.getErrcode() != 0) {
            throw new AppException("获取微信服务器的IP地址列表失败!");
        }
        return rs;
    }

    public Object menuCreate(CreateMenuInfo button) throws AppException {
        ErrorModel rs = rut.menuCreate(button);
        if (rs.getErrcode() != null && rs.getErrcode() != 0) {
            throw new AppException("创建自定义菜单失败!");
        }
        return rs;
    }

    public Object menuGet() throws AppException {
        MenuGetModel rs = rut.menuGet();
        if (rs.getErrcode() != null && rs.getErrcode() != 0) {
            throw new AppException("自定义菜单查询失败!");
        }
        return rs;
    }

    public Object menuDelete() throws AppException {
        ErrorModel rs = rut.menuDelete();
        if (rs.getErrcode() != null && rs.getErrcode() != 0) {
            throw new AppException("自定义菜单删除失败!");
        }
        return rs;
    }

    public Object getCurrentSelfmenuInfo() throws AppException {
        SelfMenuInfoModel rs = rut.getCurrentSelfmenuInfo();

        if (rs.getErrcode() != null && rs.getErrcode() != 0) {
            throw new AppException("获取微信自定义菜单配置失败!");
        }

        return rs;
    }

}
