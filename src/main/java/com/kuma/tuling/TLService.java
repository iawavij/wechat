/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.tuling;

import com.alibaba.fastjson.JSONObject;
import com.kuma.wechat4j.model.message.req.BaseRequestMessage;
import com.kuma.wechat4j.model.message.resp.BaseResponseMessage;
import com.kuma.wechat4j.model.message.resp.NewsResponseMessage;
import com.kuma.wechat4j.model.message.resp.TextResponseMessage;
import com.kuma.wechat4j.model.message.resp.media.Article;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kuma
 */
@Component
public class TLService {

    private static final Logger LOG = Logger.getLogger(TLService.class);

    @Value("${tuling.APIkey}")
    String apikey;
    @Value("${tuling.secret}")
    String secret;

    private static final String URL = "http://www.tuling123.com/openapi/api";

    public BaseResponseMessage post(TLRequest tlRequest, BaseRequestMessage requestMessage) {
        TLResponse tlResponse = request(tlRequest);

        BaseResponseMessage message;

        Long type = tlResponse.getCode();
        if (Type.text.equals(type)) {
            message = reText(tlResponse, requestMessage);
        } else if (Type.link.equals(type)) {
            message = reLink(tlResponse, requestMessage);
        } else if (Type.news.equals(type)) {
            message = reNews(tlResponse, requestMessage);
        } else if (Type.menu.equals(type)) {
            message = reMenu(tlResponse, requestMessage);
        } else if (Type.song.equals(type)) {
            message = reText(tlResponse, requestMessage);
        } else if (Type.poetry.equals(type)) {
            message = reText(tlResponse, requestMessage);
        } else {
            message = reText(tlResponse, requestMessage);
        }

        return message;
    }

    private BaseResponseMessage reMenu(TLResponse tlResponse, BaseRequestMessage requestMessage) {
        NewsResponseMessage news = new NewsResponseMessage(requestMessage);
        List<News> rows = tlResponse.getList();
        news.setArticleCount(rows.size() > 10 ? 10 : rows.size());
        List<Article> articles = new ArrayList<>();
        news.setArticles(articles);

        for (int i = 0; i < (rows.size() > 10 ? 10 : rows.size()); i++) {
            Article article = new Article();
            articles.add(article);

            article.setTitle(rows.get(i).getName());
            article.setUrl(rows.get(i).getDetailurl());
            article.setPicUrl(rows.get(i).getIcon());
            article.setDescription(rows.get(i).getInfo());
        }

        return news;
    }

    private BaseResponseMessage reNews(TLResponse tlResponse, BaseRequestMessage requestMessage) {
        NewsResponseMessage news = new NewsResponseMessage(requestMessage);
        List<News> rows = tlResponse.getList();
        news.setArticleCount(rows.size() > 10 ? 10 : rows.size());
        List<Article> articles = new ArrayList<>();
        news.setArticles(articles);

        for (int i = 0; i < (rows.size() > 10 ? 10 : rows.size()); i++) {
            Article article = new Article();
            articles.add(article);
            article.setTitle(rows.get(i).getArticle());
            article.setUrl(rows.get(i).getDetailurl());
            article.setPicUrl(rows.get(i).getIcon());
            article.setDescription("来源：" + rows.get(i).getSource());
        }

        return news;
    }

    private BaseResponseMessage reLink(TLResponse tlResponse, BaseRequestMessage requestMessage) {
        TextResponseMessage text = new TextResponseMessage(requestMessage);

        StringBuilder sb = new StringBuilder();
        sb.append(tlResponse.getText()).append("\n");
        sb.append(tlResponse.getUrl());

        text.setContent(sb.toString());
        return text;
    }

    private BaseResponseMessage reText(TLResponse tlResponse, BaseRequestMessage requestMessage) {
        TextResponseMessage text = new TextResponseMessage(requestMessage);
        text.setContent(tlResponse.getText());
        return text;
    }

    public TLResponse request(TLRequest request) {
        request.setKey(apikey);
        String str = execute(URL, request);
        return JSONObject.parseObject(str, TLResponse.class);
    }

    private String execute(String url, TLRequest params) {
        String paramsStr = JSONObject.toJSONString(params);
        LOG.debug("POST url:" + url);
        LOG.debug("params:" + paramsStr);

        String rs = null;
        Request request = Request.Post(url)
                .bodyString(JSONObject.toJSONString(params), ContentType.APPLICATION_JSON);
        try {
            HttpEntity entity = request.execute()
                    .returnResponse().getEntity();
            if (entity != null) {
                String contentType = entity.getContentType().getValue().split("=")[1];
                rs = EntityUtils.toString(entity);
                rs = new String(rs.getBytes(contentType), Consts.UTF_8);
            }
        } catch (IOException ex) {
            LOG.error("POST请求异常，" + ex.getMessage() + "\npost url:" + url);
        }

        LOG.debug("POST 请求结果:" + rs);
        return rs;
    }

}
