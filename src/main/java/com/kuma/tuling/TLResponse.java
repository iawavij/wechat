/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.tuling;

import java.util.List;

/**
 *
 * @author Kuma
 */
public class TLResponse {

    private Long code; //100000文本类，200000链接类，302000新闻类，308000菜谱类，313000（儿童版）儿歌类，314000（儿童版）诗词类
    private String text; //提示语
    private String url; //链接地址

    private List<News> list; //新闻、菜谱等列表

    private Song function;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<News> getList() {
        return list;
    }

    public void setList(List<News> list) {
        this.list = list;
    }

    public Song getFunction() {
        return function;
    }

    public void setFunction(Song function) {
        this.function = function;
    }

}
