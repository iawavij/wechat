/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.tuling;

/**
 *
 * @author Kuma
 */
public class TLRequest {

    private String key; //注册之后在机器人接入页面获得
    private String info; //请求内容，编码方式为 UTF-8
    private String loc; //位置信息，请求跟地理位置相关的内容时使用，编码方式 UTF-8
    private String userid; //开发者给自己的用户分配的唯一标志（对应自己的每一个用户）

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
