/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.tuling;

/**
 *
 * @author Hx
 */
public enum Type {
    text(100000L), //文本类
    link(200000L), //链接类
    news(302000L), //新闻类
    menu(308000L), //菜谱类
    song(313000L), //儿歌类
    poetry(314000L), //诗词类
    key(40001L), //参数 key 错误
    info(40002L), //请求内容 info 为空
    over(40004L), //当天请求次数已使用完
    format(40007L); //数据格式异常

    // 定义私有变量
    private final Long nCode;

    // 构造函数，枚举类型只能为私有
    private Type(Long _nCode) {
        this.nCode = _nCode;
    }

    @Override
    public String toString() {
        return String.valueOf(this.nCode);
    }

    public boolean equals(Long code) {
        return code.equals(this.nCode);
    }
}
