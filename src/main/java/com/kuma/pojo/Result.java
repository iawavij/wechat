/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.pojo;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Kuma
 */
public class Result extends HashMap<String, Object> {

    private final String suString = "success";
    private final String msgString = "message";

    public Result() {
        this(true);
    }

    public Result(boolean success) {
        this.put(suString, success);
    }

    public Result(boolean success, String message) {
        this.put(suString, success);
        this.put(msgString, message);
    }

    public boolean isResult() {
        return (boolean) this.get(suString);
    }

    public void setResult(boolean result) {
        this.put(suString, result);
    }

    public String getMessage() {
        return (String) this.get(msgString);
    }

    public void setMessage(String message) {
        this.put(msgString, message);
    }

    public static Result success() {
        return new Result(true);
    }

    public static Result success(String message) {
        return new Result(true, message);
    }

    public static Result error() {
        return new Result(false);
    }

    public static Result error(String message) {
        return new Result(false, message);
    }

    public Result arg(String key, Object value) {
        this.put(key, value);
        return this;
    }

    public Result args(Map<?, ?> args) {
        if (args != null) {
            for (Map.Entry<?, ?> entrySet : args.entrySet()) {
                Object key = entrySet.getKey();
                Object value = entrySet.getValue();
                this.put(key.toString(), value);
            }
        }
        return this;
    }
}
